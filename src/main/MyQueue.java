/**
 * Copyright � 2015 Ntq-Solution.
 * 
 */
package main;

import java.awt.List;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

class Node{
	int value;
	Node next;
	public Node(int v) {
		value=v;
		next=null;
	}
}

public class MyQueue{
	private Node first;
	private Node last;
	
	public MyQueue() {
		this.first = null;
		this.last = null;
	}
	
	/* 
	 * to do
	 */
	
	public synchronized boolean add(Integer e) {
		if(first == null){
			Node newNode = new Node(e);
			first = newNode;
			last = newNode;
			return true;
		}
		
		if(first != null){
			Node newnode = new Node(e);
			first.next = newnode;
			first = newnode;
			return true;
		}
		return false;
		
	}
	
	/* to do
	 * return the value of the first element of the queue
	 * not remove
	 */
	
	public Integer peek() {
		return last.value;
	}
	
	/* to do
	 * return the value of the first element of the queue
	 * remove
	 */
	
	public synchronized Integer poll() {
		int a = last.value;
		if(first == last){
			last = null;
			first = null;
		}
		else{
			last = last.next;
		}
		return a;
	}

	/* to do
	 * size of queue
	 */
	
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* to do
	 */
	
	public boolean isEmpty() {
		if(first == null) 
			return true;
		
		return false;
	}

	/* to do
	 */
	
	public boolean contains(Integer o) {
		Node currentNode = last;
		while(currentNode!=null){
			if(currentNode.value == o) return true;
			currentNode = currentNode.next;
		}
		
		return false;
	}
	/* (non-Javadoc)
	 * @see java.util.Collection#addAll(java.util.Collection)
	 */
	
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#removeAll(java.util.Collection)
	 */
	
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#iterator()
	 */
	
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#toArray()
	 */
	
	public Integer[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#toArray(java.lang.Integer[])
	 */
	
	public Integer[] toArray(Integer[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#remove(java.lang.Integer)
	 */
	
	public boolean remove(Integer o) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 */
	
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	

	/* (non-Javadoc)
	 * @see java.util.Collection#retainAll(java.util.Collection)
	 */
	
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#clear()
	 */
	
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	

	/* (non-Javadoc)
	 * @see java.util.Queue#offer(java.lang.Integer)
	 */
	public boolean offer(Integer e) {
		// TODO Auto-generated method stub
		return false;
	}

	

	/* (non-Javadoc)
	 * @see java.util.Queue#element()
	 */
	
	public Integer element() {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
