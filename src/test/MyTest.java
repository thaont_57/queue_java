/**
 * Copyright � 2015 Ntq-Solution.
 * 
 */
package test;

import static org.junit.Assert.assertTrue;
import main.MyQueue;

import org.junit.Before;
import org.junit.Test;


public class MyTest {

	private MyQueue queue;
	
	@Before
	//this method will be run before each '@Test' due to the '@Before' annotation.
	//use it to initialize data structures, and other things you need to do before each test 
	public void init()
	{
		queue=new MyQueue();
	}
	
	@Test
	//the '@Test' annotation marks it as, surprisingly enough, a test.
	//each test will be run independently 
	public void testIsEmptyOnEmpty() {
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testIsEmptyOnNotEmpty() {
		queue.add(5);
		assertTrue(!queue.isEmpty());
	}
	
	@Test
	public void testOneadd() {
		queue.add(5);
		queue.add(6);
		queue.add(7);
		assertTrue(5==queue.peek());
	}
	
	@Test
	public void testOnepoll() {
		queue.add(5);
		assertTrue(5==queue.poll());
	}
	
	@Test
	public void testOnepeek() {
		queue.add(5);
		assertTrue(5==queue.peek());
	}
	
	@Test
	public void testOrdering() {
		queue.add(1);
		queue.add(2);
		queue.add(3);
		assertTrue(1== queue.poll());
		assertTrue(2==queue.poll());
		assertTrue(3==queue.poll());
	}
	
	@Test
	public void testCurrentValue() {
		queue.add(1);
		queue.add(2);
		queue.add(3);
		assertTrue(queue.contains(3));
		assertTrue(!queue.contains(7));
	}
	
	@Test
	//this checks to make sure that adding then polling doesn't break isEmpty()
	public void testEmptyAfterpoll() {
		queue.add(5);
		queue.poll();
		assertTrue(queue.isEmpty());
	}
}